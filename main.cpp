/*
 * Szczepanczyk Dominik
 */



#include <cstdlib>
#include <stdio.h>
#include <cstring>
#include "time.h"
#include "algorithm"
#include "/usr/include/GL/gl.h"
#include "/usr/include/GL/freeglut.h"

using namespace std;

extern void Pralka();
extern void Beben();
extern void Przycisk(float x, float y, float z);

int x = 0;
int katX = 290;
int katY = 1;
int katZ = 340;
int timeValue = 100;
int speedX = 0;
float wspolczynnikMigania = 0;
short ktoryMiga = 0;
short ktoryMiga2 = 0;
bool fade = false;
bool speedUp = true;


void Inicjuj()
{
    glMatrixMode ( GL_PROJECTION );  
    glLoadIdentity();  
    glOrtho(-10,10,-10,10,-100.0,100.0);
    
 glMatrixMode( GL_MODELVIEW );
 
 glLoadIdentity();
 
 glClearColor( 1.0, 1.0,1.0, 1.0 );
 glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
 glRotatef(katX,0,1,0);
 glRotatef(katY,1,0,0);
 glRotatef(katZ,0,0,1);
 glEnable( GL_LIGHT0);
 glEnable( GL_LIGHTING);
 glEnable( GL_NORMALIZE );
 glEnable(GL_DEPTH_TEST);
 glShadeModel(GL_SMOOTH);
 
 GLfloat ambient_light[4] =
	{
		0.3, 0.3, 0.3, 1.0
	};
 
        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient_light);
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT);
	static GLfloat lightPosition[4] = {5, 10, 5, 1.0 };
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    
}

void RysujPrzyciski(float miganie)
{
     glPushMatrix();
 
 glTranslatef(1.504,4.4,-4.6);
 if(ktoryMiga == 0)
 glColor3f(1 * miganie,1 * miganie,1 * miganie);
 else
 glColor3f(1,1,1);
 Przycisk(0.1,0.4,0.4);
 

 glTranslatef(0,0,1);
 if(ktoryMiga == 1)
 glColor3f(1 * miganie,1 * miganie,0);
 else
     glColor3f(1,1,0);
 Przycisk(0.1,0.4,0.4);


 glTranslatef(0,0,1);
 if(ktoryMiga == 2)
 glColor3f(1 * miganie,0,0);
 else
     glColor3f(1,0,0);
 Przycisk(0.1,0.4,0.4);
 
 
 glTranslatef(0,0,1);
 if(ktoryMiga == 3 || ktoryMiga2 == 3)
 glColor3f(1 * miganie,0,1 * miganie);
 else
     glColor3f(1,0,1);
 Przycisk(0.1,0.4,0.4);
 
 
 glTranslatef(0,-1,-3);
 if(ktoryMiga == 4 || ktoryMiga2 == 4)
 glColor3f(0,1 * miganie,1 * miganie);
 else
     glColor3f(0,1,1);
 Przycisk(0.1,0.4,0.4);
 
 
 glTranslatef(0,0,1);
 if(ktoryMiga == 5)
 glColor3f(0,0,1 * miganie);
 else
     glColor3f(0,0,1);
 Przycisk(0.1,0.4,0.4);
 
 
 glTranslatef(0,0,1);
 if(ktoryMiga == 6 || ktoryMiga2 == 6)
 glColor3f(miganie,miganie,miganie/2);
 else
     glColor3f(0,0,0);
 Przycisk(0.1,0.4,0.4);
 
 
 glTranslatef(0,0,1);
 glColor3f(0,1 * miganie,0);
 Przycisk(0.1,0.4,0.4);
 
 glPopMatrix();
}

void RysujPralke()
{
 glPushMatrix();
 glTranslatef(0,-0.241554,-0.23721);
 Pralka();
 glPopMatrix();
}

void RysujBeben()
{
  glPushMatrix(); 
  glRotatef(x,1,0,0);
  glTranslatef(0,-0.228468,-0.23721);
  Beben();
  glPopMatrix();
}


void RysujUklad()
{
      glPushMatrix();
    
        glBegin(GL_LINES);
        glVertex3f(-10,0,0);
        glVertex3f(10,0,0);
        glEnd();
        
        glBegin(GL_LINES);
        glVertex3f(0,10,0);
        glVertex3f(0,-10,0);
        glEnd();
        
        glBegin(GL_LINES);
        glVertex3f(0,0,10);
        glVertex3f(0,0,-10);
        glEnd();     
        
        glPopMatrix();
}

void ObslugaSilnika()
{
    x++;
  x+= speedX;
  if( x > 359)
  {
      x = 0;
  
      if(speedUp)
          speedX++;
      else
          speedX--;
  }
  
  if(speedX == 15)
      speedUp = false;
  else if(speedX == 0)
      speedUp = true;
  
  if( timeValue == 1)
  {
     timeValue = 1; 
  }
  else
  {
      timeValue--;
  }
  if(!fade)
  wspolczynnikMigania+=0.05;
  else
  wspolczynnikMigania-=0.05;
  
  if(wspolczynnikMigania > 1)
  {
      fade = true;
  }
  else if( wspolczynnikMigania < 0)
  {
      fade = false;
  }
  
  if(katX == 0)
      katX = 360;
  else if(katX > 360)
      katX = 1;
  if(katY == 0)
      katY = 360;
  else if(katY > 360)
      katY = 1;
  if(katZ == 0)
      katZ = 360;
  else if(katZ > 360)
      katZ = 1;
  
  srand( time(0) );
  
  ktoryMiga = rand () % 8;
  ktoryMiga2 = rand() % 8;
}


void 
Rysuj(void)
{
  Inicjuj();    
  RysujUklad();
  RysujBeben();
  RysujPralke();
  RysujPrzyciski(wspolczynnikMigania);
  ObslugaSilnika();
 
  glFlush();
 glutSwapBuffers();
 
}

void Timer( int value )
{
    
    Rysuj();

    
    glutTimerFunc(timeValue, Timer,0 );
}

void klawiatura(unsigned char key, int x, int y)
{
    if( key == 'w')
    {
       if(katX / 90 == 1 || katX / 90 == 3)
           katZ++;
       else
           katY++;
    }
    else if( key == 's' )
    {
 
               if(katX / 90 == 1 || katX / 90 == 3)
           katZ--;
       else
           katY--;
    }
    else if( key == 'a')
    {
         katX--;
    }
    else if( key == 'd')
    {
         katX++;
    }
   
    
}

void Reshape(int x, int y)
{
    int min = std::min(x, y);
    
    glViewport(0,0,min,min);
    
    Rysuj();
}

int 
main(int argc, char **argv)
{

   glutInitWindowSize(500, 500);
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_RGB|GLUT_DOUBLE|GLUT_DEPTH);

   glutCreateWindow("Pralka");

   glutDisplayFunc(Rysuj);
   glutKeyboardFunc(klawiatura);
   glutReshapeFunc(Reshape);
   glutTimerFunc(timeValue, Timer,0);
   glutMainLoop();
   
   return 0;
}